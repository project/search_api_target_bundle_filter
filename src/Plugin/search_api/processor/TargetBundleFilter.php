<?php

namespace Drupal\search_api_target_bundle_filter\Plugin\search_api\processor;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Item\FieldInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\SearchApiException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filter a reference field's value by the bundle of the target entities.
 *
 * @SearchApiProcessor(
 *   id = "target_bundle_filter",
 *   label = @Translation("Target Bundle Filter"),
 *   description = @Translation("Provides a filter such that values from only selected target bundles of a field will be included in the results."),
 *   stages = {
 *     "preprocess_index" = -45
 *   }
 * )
 */
class TargetBundleFilter extends ProcessorPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * Static cache for getReferenceFields() return values, keyed by index ID.
   *
   * @var string[][][]
   *
   * @see \Drupal\search_api_target_bundle_filter\Plugin\search_api\processor\TargetBundleFilter::getReferenceFields()
   */
  protected static array $indexReferenceFields = [];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;


  /**
   * The entity bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $processor->setEntityTypeManager($container->get('entity_type.manager'));
    $processor->setEntityTypeBundleInfo($container->get('entity_type.bundle.info'));
    $processor->setEntityFieldManager($container->get('entity_field.manager'));

    return $processor;
  }

  /**
   * Retrieves the entity type manager service.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager service.
   */
  public function getEntityTypeManager() {
    return $this->entityTypeManager ?: \Drupal::entityTypeManager();
  }

  /**
   * Sets the entity type manager service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   * @return $this
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    return $this;
  }

  /**
   * Retrieves the entity display repository.
   *
   * @return \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   *   The entity display repository.
   */
  public function getEntityTypeBundleInfo() {
    return $this->entityTypeBundleInfo ?: \Drupal::service('entity_type.bundle.info');
  }

  /**
   * Sets the entity type bundle info.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The new entity type bundle info.
   *
   * @return $this
   */
  public function setEntityTypeBundleInfo(EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    return $this;
  }

  /**
   * Retrieves the entity field manager.
   *
   * @return \Drupal\Core\Entity\EntityFieldManagerInterface
   *   The entity field manager.
   */
  public function getEntityFieldManager() {
    return $this->entityFieldManager ?: \Drupal::service('entity_field.manager');
  }

  /**
   * Sets the entity field manager.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The new entity field manager.
   *
   * @return $this
   */
  public function setEntityFieldManager(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function supportsIndex(IndexInterface $index) {
    $processor = new static(['#index' => $index], 'target_bundle_filter', []);
    foreach ($processor->getReferenceFields() as $field_options) {
      if (count($field_options) > 0) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Finds all reference fields for this processor's index.
   *
   * Fields are returned if:
   * - They point to an entity type
   * - They reference more than one bundle.
   *
   * @return string[][]
   *   An array containing all fields of the index for which target data
   *   might be retrievable. The keys are those field's IDs, the values are
   *   associative arrays containing the allowed bundles of those fields.
   */
  protected function getReferenceFields() {
    if (!isset(static::$indexReferenceFields[$this->index->id()])) {
      $bundle_info = $this->getEntityTypeBundleInfo()->getAllBundleInfo();
      $field_options = [];
      foreach ($this->index->getFields() as $field_id => $field) {
        try {
          $property = $field->getDataDefinition();
          $datasource = $field->getDatasource();
        }
        catch (SearchApiException $e) {
          $this->logError($field, $e);
          continue;
        }
        if (!$datasource) {
          continue;
        }
        if ($property->getDataType() !== 'field_item:entity_reference') {
          continue;
        }

        $entity_type_id = $datasource->getEntityTypeId();
        // Field instances per bundle can reference different targets, so we
        // add them all to start.
        $bundles = $bundle_info[$entity_type_id];
        if (count($bundles) == 1) {
          // No need to filter if there is only one bundle.
          continue;
        }
        $datasource_plugin = $datasource->getPluginId();
        $field_options += [$datasource_plugin => []];
        // For every bundle.
        foreach ($bundles as $bundle_id => $bundle) {
          $bundle_fields = $this->getEntityFieldManager()
            ->getFieldDefinitions($entity_type_id, $bundle_id);
          // Check if this bundle has the taxonomy entity reference field.
          if (array_key_exists($property->getFieldDefinition()->getName(), $bundle_fields)) {
            $field_definition = $bundle_fields[$property->getFieldDefinition()->getName()];
            $bundle_settings = $field_definition->getSettings();
            $field_options[$datasource_plugin] += [$field_id => []];
            if (!empty($bundle_settings['handler_settings']['target_bundles'])
              && !empty($bundle_settings['target_type'])) {
              // Convert bundle options to labels.
              $options = $bundle_settings['handler_settings']['target_bundles'];
              foreach ($options as $key => $option) {
                $options[$key] = $bundle_info[$bundle_settings['target_type']][$key]['label'];
              }
              $field_options[$datasource_plugin][$field_id] += $options;
            }
          }
        }
      }
      static::$indexReferenceFields[$this->index->id()] = $field_options;
    }

    return static::$indexReferenceFields[$this->index->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();

    $configuration += [
      'tbf' => [],
    ];

    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState) {
    $form['#description'] = $this->t('Select fields to be filtered by reference target bundle.');
    $indexDatasources = $this->index->getDatasources();

    foreach ($this->getReferenceFields() as $datasource_id => $fields) {
      $datasource_configuration = $this->configuration['tbf'][$datasource_id] ?? [];
      $form['tbf'][$datasource_id] = [
        '#type' => 'details',
        '#title' => $this->t('Bundle filter settings for %datasource',
          ['%datasource' => $indexDatasources[$datasource_id]->label()]),
        '#open' => TRUE,
      ];
      foreach ($fields as $field_id => $field_options) {
        $enabled = !empty($datasource_configuration['fields'][$field_id]);
        $form['tbf'][$datasource_id]['fields'][$field_id] = [
          '#type' => 'details',
          '#title' => $this->index->getField($field_id)->getLabel(),
          '#open' => $enabled,
        ];
        $name = "processors[target_bundle_filter][settings][tbf][{$datasource_id}][fields][{$field_id}][field_status]";
        $form['tbf'][$datasource_id]['fields'][$field_id]['field_status'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Enable field?'),
          '#default_value' => $enabled ? 'on' : NULL,
          '#return_value' => 'on',
        ];
        $label_convert = !empty($datasource_configuration['fields'][$field_id]['convert_to_labels']);
        $form['tbf'][$datasource_id]['fields'][$field_id]['convert_to_labels'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Convert field values from id to label?'),
          '#default_value' => $label_convert ? 'label' : NULL,
          '#return_value' => 'label',
          '#states' => [
            // Show this checkbox only if the field is enabled above.
            'visible' => [
              ':input[name="' . $name . '"]' => ['checked' => TRUE],
            ],
          ],
        ];
        $form['tbf'][$datasource_id]['fields'][$field_id]['selection_type'] = [
          '#type' => 'radios',
          '#title' => $this->t('Which bundles should be indexed?'),
          '#options' => [
            'only' => $this->t('Only those selected'),
            'all_except' => $this->t('All except those selected'),
          ],
          '#access' => count($field_options) > 0,
          '#states' => [
            // Show this checkbox only if the field is enabled above.
            'visible' => [
              ':input[name="' . $name . '"]' => ['checked' => TRUE],
            ],
          ],
          '#default_value' => $datasource_configuration['fields'][$field_id]['selection_type'] ?? 'all_except',
        ];

        $default_bundles = $enabled ? $datasource_configuration['fields'][$field_id]['selected_bundles'] : [];
        $form['tbf'][$datasource_id]['fields'][$field_id]['selected_bundles'] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('Select bundles'),
          '#description' => $this->t("Please pick the bundles that should be indexed."),
          '#options' => $field_options,
          '#default_value' => $default_bundles,
          '#size' => min(4, count($field_options)),
          '#multiple' => TRUE,
          '#access' => count($field_options) > 0,
          '#states' => [
            // Show this checkbox only if the field is enabled above.
            'visible' => [
              ':input[name="' . $name . '"]' => ['checked' => TRUE],
            ],
          ],
        ];
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    foreach ($this->index->getDatasourceIds() as $datasource_id) {
      foreach ($this->index->getFieldsByDatasource($datasource_id) as $field_id => $fieldInterface) {
        $enabled = $values['tbf'][$datasource_id]['fields'][$field_id]['field_status'] ?? '';
        if ($enabled !== "on") {
          $values['tbf'][$datasource_id]['fields'][$field_id] = [];
          continue;
        }
        $field_values = $values['tbf'][$datasource_id]['fields'][$field_id];

        $values['tbf'][$datasource_id]['fields'][$field_id]['field_status'] =
          ($field_values['field_status'] === "on") ? 1 : 0;
        $values['tbf'][$datasource_id]['fields'][$field_id]['convert_to_labels'] =
          ($field_values['convert_to_labels'] === "label") ? 1 : 0;
        $values['tbf'][$datasource_id]['fields'][$field_id]['selection_type'] =
          $field_values['selection_type'] ?? 'all_except';
        $values['tbf'][$datasource_id]['fields'][$field_id]['selected_bundles'] =
          array_keys(array_filter($field_values['selected_bundles'],
            function ($a) {
              return $a !== 0;
            }));
      }
    }
    $form_state->setValues($values);
    $this->setConfiguration($values);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $tbf_settings = $form_state->getValue('tbf');
    $found_config = FALSE;
    foreach ($tbf_settings as $field_options) {
      foreach ($field_options['fields'] as $field_id => $field_values) {
        if (!empty($field_values['field_status'])) {
          if (empty($field_values['selection_type'])) {
            $form_state->setError($form['fields'][$field_id]['selection_type'],
              $this->t('You need to select how to filter bundles for each enabled field.'));
          }
          else {
            $found_config = TRUE;
            break;
          }
        }
      }
    }

    $form_state->setValue('tbf', $tbf_settings);
    if (!$found_config) {
      $form_state->setError($form['fields'],
        $this->t('You need to enable target filter on at least one field or disable the processor.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preprocessIndexItems(array $items) {

    /** @var \Drupal\search_api\Item\ItemInterface $item */
    foreach ($items as $item) {
      $datasource_id = $item->getDatasourceId();
      foreach ($this->configuration['tbf'][$datasource_id]['fields'] as $field_id => $field_config) {
        if (empty($field_config) || ($field_config['field_status'] ?? FALSE) === FALSE) {
          continue;
        }
        /** @var \Drupal\search_api\Item\FieldInterface $field */
        $field = $item->getField($field_id);
        if (!$field) {
          continue;
        }

        try {
          $this->filterBundles($field, $item, $field_config);
        }
        catch (\Exception $e) {
          $this->logIndexError($field, $e);
          continue;
        }
      }
    }
  }

  /**
   * Perform the main work of filtering to selected bundles.
   *
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   The search api field to filter.
   * @param \Drupal\search_api\Item\ItemInterface $item
   *   The search api item to update values on.
   * @param array $field_config
   *   The saved configuration for this field.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function filterBundles(FieldInterface $field,
                                      ItemInterface $item,
                                      array $field_config) {
    try {
      $object = $item->getOriginalObject();
    }
    catch (SearchApiException $e) {
      return;
    }

    $path = $field->getPropertyPath();
    if (!$object->getEntity()->hasField($path)) {
      return;
    }
    $original_field = $object->get($path);

    if ($original_field->isEmpty() || !method_exists($original_field, 'referencedEntities')) {
      return;
    }

    $references = $original_field->referencedEntities();
    // Filter $references down to wanted bundles and index by id.
    $ids = [];
    // Do we want "only" certain bundles or "all except".
    $filter_to_these = $field_config['selection_type'] == "only";
    /** @var \Drupal\Core\Entity\EntityInterface  $reference */
    foreach ($references as $reference) {
      $selected_bundles = $field_config['selected_bundles'] ?? [];
      if (in_array($reference->bundle(), $selected_bundles) === $filter_to_these) {
        $ids[$reference->id()] = $reference->label();
      }
    }
    // Check each field value against filtered values.
    $values = count($ids) ? $field->getValues() : [];
    foreach ($values as $delta => $entity_id) {
      if (array_key_exists($entity_id, $ids) === FALSE) {
        unset($values[$delta]);
      }
      elseif ($field_config['convert_to_labels'] == TRUE) {
        $values[$delta] = $ids[$entity_id];
      }
    }
    $field->setValues($values);
  }

  /**
   * Log an exception while processing the index.
   *
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   The field causing the error.
   * @param \Exception $e
   *   The exception to log.
   */
  protected function logIndexError(FieldInterface $field, \Exception $e): void {
    $vars = [
      '%index' => $this->index->label(),
      '%field' => $field->getLabel(),
      '%field_id' => $field->getFieldIdentifier(),
    ];
    watchdog_exception('search_api', $e, '%type while trying to filter bundle values for field %field (%field_id) on index %index: @message in %function (line %line of %file).', $vars);
  }

  /**
   * Log an exception when retrieving reference field properties.
   *
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   The field causing the error.
   * @param \Exception $e
   *   The exception to log.
   */
  protected function logError(FieldInterface $field, \Exception $e): void {
    $vars = [
      '%index' => $this->index->label(),
      '%field' => $field->getLabel(),
      '%field_id' => $field->getFieldIdentifier(),
    ];
    watchdog_exception('search_api', $e, '%type while trying to determine bundle values for field %field (%field_id) on index %index: @message in %function (line %line of %file).', $vars);
  }

}
