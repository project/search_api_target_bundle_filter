CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

The Search API Target Bundle Filter module allows a site builder to filter which
bundles from a reference field targeting multiple bundles are indexed in a
search_api field. Basically this allows you to create separate features per
bundle in your search index that can be used as facets even if the selected
reference field holds multiple bundles.

It does this by adding a processor to the index.
The processor can be enabled/disabled per field.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/search_api_target_bundle_filter

REQUIREMENTS
------------

This module requires the following modules:

* [Search API](https://www.drupal.org/project/search_api)

RECOMMENDED MODULES
-------------------

* [Facets](https://www.drupal.org/project/facets):
  With Search API target bundle filter facets can be created per bundle
  or just for a few bundles.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

* Configure the search api processor on your index at
  Administration » Configuration » Search API » (Your Index) » Processors:

  - Enable selected bundles per search api field.
  - To make a facet from each bundle you will need to add the same field to the
    index again.
  - If Index Hierarchy is needed it will need to come before this processor.

MAINTAINERS
-----------

Current maintainers:
* Lola Slade (lolcode) - https://www.drupal.org/u/lolcode

This project has been sponsored by:
* Registered Nurses Association of Ontario (RNAO)
  - https://www.drupal.org/registered-nurses-association-of-ontario
